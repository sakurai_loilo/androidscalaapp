package tv.loilo.ScalaApp

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView


/**
 * Created by sakurai on 2014/09/26.
 */
class MainActivity extends Activity {
  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
//    setContentView(R.layout.main)
    val view = getLayoutInflater().inflate(R.layout.main, null)
    val textView = view.findViewById(R.id.text_view).asInstanceOf[TextView]
    textView.setText("Hello Loilo!")
    setContentView(view)
    Log.d("hoge", "Hello From Scala")
  }
}
