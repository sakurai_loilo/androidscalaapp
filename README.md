# README #

ScalaでAndroidアプリを作るためのテストリポジトリです

# 環境

基本的に2014/09/26時点で最新のものを使っています

- OSX 10.9
- Android SDK 23
- sbt 0.13.5
- JDK 1.8.0_20
- Java SE 8
- Scala 2.11.1

その他

- VirtualBox
- Genymotion

## セットアップ

### 必要な物を全てbrewでinstall

```shell

brew update
brew install android-sdk sbt scala

```

### Java8を入れる

JDK1.8を使うのでMacに入っていなければ以下からインストールする。

[http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html]()

以下のコマンドで自分のMacに入っているJavaを確認できる

```zsh
/usr/libexec/java_home -V   
```

こんな感じになっていればおそらく大丈夫

```
Matching Java Virtual Machines (3):
    1.8.0_20, x86_64:	"Java SE 8"	/Library/Java/JavaVirtualMachines/jdk1.8.0_20.jdk/Contents/Home
    1.6.0_65-b14-462, x86_64:	"Java SE 6"	/System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home
    1.6.0_65-b14-462, i386:	"Java SE 6"	/System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home

/Library/Java/JavaVirtualMachines/jdk1.8.0_20.jdk/Contents/Home
```

### Genymotion on Virtualboxを入れる

Androidのエミュレータが必要なので入れる

[http://www.genymotion.com/]()

### IntelliJ IDEAを入れる

[http://www.jetbrains.com/idea/]()

### IntelliJのセットアップをする

以下の記事を参考に  
[http://qiita.com/takudo/items/0a9c474a952acfbdabfc]()

### プロジェクトをビルドする

コマンドラインからプロジェクトにいって

```
sbt
```

これで必要なsbtプラグインがインストールされる
そのままsbtコンソールで
```
> android:package-debug
```
と打つとプロジェクトがビルドされる
```
> ~android:package-debug
```
チルダを頭につけるとwatchしつつビルドしてくれるようになる

### Genymotionで起動する

Genymotionで適当なデバイスをインストールして起動しておき、IntelliJのDebugからデバイスを選んで起動する